#!/bin/bash

pyfunc () {
  if hash pythonw 2>/dev/null; then
    pythonw "$@"
  else
    python "$@"
  fi
}

## Script to run comparison of ESS and ISIS TS2
neutrons=1e8

## Clean up
rm -fr ess-isis-compare-tmp

## Set up
git clone https://bitbucket.org/essloki/loki-mcstas lmtmp
cp -R lmtmp/ess-isis-compare-tmp .
rm -fr lmtmp

cd ess-isis-compare-tmp

## Run tests in parallel
## ESS
mcrun -n $neutrons ess-simple.instr l_min=1.0 l_max=20.0 >"ess-simple.log" 2>&1 &
## ISIS
mcrun -n $neutrons isis-simple.instr l_min=1.0 l_max=20.0 >"isis-simple.log" 2>&1 &
echo "Running simulations"
wait
echo "Plotting results"
pyfunc "compare-ess-isis.py"
