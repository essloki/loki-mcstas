# -*- coding: utf-8 -*-
from matplotlib import pylab as plt
import numpy as np
import os
import glob

#Get most recent files
newest_ess = max(glob.iglob('ess-simple_*/*.L'), key=os.path.getmtime)
newest_isis = max(glob.iglob('isis-simple_*/*.L'), key=os.path.getmtime)

wavelength,isis_intensity,isis_intensity_err = np.loadtxt(newest_isis,unpack=True,usecols=(0,1,2))

ess_intensity,ess_intensity_err = np.loadtxt(newest_ess,unpack=True,usecols=(1,2))


fig = plt.figure(figsize=(11.7,8.3))
plt.semilogy(wavelength,isis_intensity,label="ISIS TS2 ("+newest_isis+")")
plt.semilogy(wavelength,ess_intensity, label="ESS ("+newest_ess+")")

plt.semilogy(wavelength,ess_intensity/isis_intensity,label = "Ratio ESS/ISIS\n  Mean :"+str(int(np.mean(ess_intensity/isis_intensity)))+"\n"
                                                                "  Min :"+str(int(np.min(ess_intensity/isis_intensity)))+"\n"
                                                                "  Max :"+str(int(np.max(ess_intensity/isis_intensity))))


plt.title("Comparison of TS2 hydrogen moderator with ESS moderator")

plt.legend(fontsize='small')
plt.xlabel('Wavelength (A)')
plt.ylabel('neutrons/second')
plt.grid()
plt.show()
