/*******************************************************************************
*         mcstas instrument definition url=http://www.mcstas.org
*
* instrument: isis-simple
*
* %identification
* written by: andrew jackson (andrew.jackson@esss.se)
* date: 2016-02-25
* origin: ess
* release: mcstas 2.2a
* version: 1.0
* %instrument_site: ess
*
* loki
*
* %description
* Simple model to represent ISIS for comparison with ISIS
*
* Example:
*
*******************************************************************************/


define instrument loki(double l_min=1.0, double l_max=20.0
                        )

declare
%{

%}

initialize
%{

%}

TRACE

COMPONENT Origin = Progress_bar()
  AT (0,0,0) ABSOLUTE

	COMPONENT Source = ISIS_moderator(
	    Face = "hydrogen", Emin = -l_max, Emax = -l_min, dist = 2.0,
	    focus_xw = 0.03, focus_yh = 0.03, xwidth = 0.074, yheight = 0.074,
	    CAngle = 0.0, SAC = 1)
  AT (0,0,0) RELATIVE Origin

//Define an instrument arm that can then be tilted
//Does not match real geometry but will do here.
COMPONENT InstrumentArm = Arm()
	AT (0,0,2.0) RELATIVE Source
	ROTATED (0,0,0) RELATIVE Source

//Set this component as the target of focus of the moderator component
COMPONENT ap1 = Slit(
	radius = 0.015
)
AT (0,0,0.0) RELATIVE InstrumentArm

COMPONENT ap2 = Slit(
	radius = 0.015
)
AT (0,0,4.0) RELATIVE PREVIOUS


COMPONENT Monitor1 = Monitor_nD(
 xwidth=0.03, yheight=0.03,
 options="lambda, time, multiple, auto",
 restore_neutron = 1
)
AT (0,0,0.001) RELATIVE PREVIOUS


FINALLY
%{

%}
END
