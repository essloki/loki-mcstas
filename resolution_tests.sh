#!/bin/bash

## Script to run LoKI resolution tests
## See https://confluence.esss.lu.se/display/loki/Detector+Testing+via+Simulations
mcplout=1
neutrons=1e10

## Clean out previous run
rm -fr resolution-test

## Set up
git clone https://bitbucket.org/essloki/loki-mcstas-master resolution-test

cd resolution-test

#compile model
mcstas loki-master-model.instr
gcc -O loki-master-model.c -o loki-master-model -lm

## Run tests in parallel
for model in 6 9 11
do
echo "Starting mcstas jobs for model $model"
## Configuration 1
  ./loki-master-model -n $neutrons -d "config_1_model_$model" \
        sampletype=$model incohb=0.00001 collen=3.0 reardet=5.0 l_min=3.0 \
        l_max=11.5 sourceapx=0.03 sampleapx=0.01 pulseskip=0 \
        mcplout=$mcplout >"config_1_model_$model.log" 2>&1 &

## Configuration 2
  ./loki-master-model -n $neutrons -d "config_2_model_$model" \
        sampletype=$model incohb=0.00001 collen=5.0 reardet=5.0 l_min=3.0 \
        l_max=11.5 sourceapx=0.02 sampleapx=0.01 pulseskip=0 \
        mcplout=$mcplout >"config_2_model_$model.log" 2>&1 &

## Configuration 3
  ./loki-master-model -n $neutrons -d "config_3_model_$model" \
        sampletype=$model incohb=0.00001 collen=8.0 reardet=10.0 l_min=3.0 \
        l_max=10.0 sourceapx=0.02 sampleapr=0.01 pulseskip=0 \
        mcplout=$mcplout >"config_3_model_$model.log" 2>&1 &

## Configuration 4
  ./loki-master-model -n $neutrons -d "config_4_model_$model" \
        sampletype=$model incohb=0.00001 collen=5.0 reardet=5.0 l_min=3.0 \
        l_max=21.4 sourceapx=0.02 sampleapx=0.01 pulseskip=1 \
        mcplout=$mcplout >"config_4_model_$model.log" 2>&1 &
echo "Waiting for mcstas jobs for model $model to finish ..."
wait
done
echo "All mcstas jobs finished ..."
#echo "processing output ..."
