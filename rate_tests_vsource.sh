#!/bin/bash

## Script to run LoKI rate tests
## See https://confluence.esss.lu.se/display/loki/Detector+Testing+via+Simulations
mcplout=1
neutrons=1e10

## Clean out previous run
rm -fr rate-test

## Set up
git clone https://bitbucket.org/essloki/loki-mcstas-master rate-test

cd rate-test

#compile full model
mcstas loki-master-model.instr
gcc -O loki-master-model.c -o loki-master-model -lm

##Generate virtual sources
echo "Starting mcstas jobs for model $model"
## Configuration 1
  ./loki-master-model -n $neutrons -d "vsource_config_1" \
        sampletype=-1 collen=3.0 reardet=5.0 l_min=3.0 \
        l_max=11.5 sourceapx=0.03 sampleapx=0.01 pulseskip=0 \
        mcplout=0 windows=0 virtualout=1 >"vsource_config_1.log" 2>&1 &

## Configuration 2
  ./loki-master-model -n $neutrons -d "vsource_config_2" \
        sampletype=-1 collen=5.0 reardet=5.0 l_min=3.0 \
        l_max=11.5 sourceapx=0.02 sampleapx=0.01 pulseskip=0 \
        mcplout=0 windows=0 virtualout=1 >"vsource_config_2.log" 2>&1 &

## Configuration 3
  ./loki-master-model -n $neutrons -d "vsource_config_3" \
        sampletype=-1 collen=8.0 reardet=10.0 l_min=3.0 \
        l_max=10.0 sourceapx=0.02 sampleapr=0.01 pulseskip=0 \
        mcplout=0 windows=0 virtualout=1 >"vsource_config_3.log" 2>&1 &

## Configuration 4
   ./loki-master-model -n $neutrons -d "vsource_config_4" \
        sampletype=-1 collen=5.0 reardet=5.0 l_min=3.0 \
        l_max=21.4 sourceapx=0.02 sampleapx=0.01 pulseskip=1 \
        mcplout=0 windows=0 virtualout=1 >"config_4_model_$model.log" 2>&1 &

echo "Waiting for virtual source generation mcstas jobs to finish ..."
wait

#compile backend model
mcstas loki-master-postSampleOnly.instr
gcc -O loki-master-postSampleOnly.c -o loki-master-postSampleOnly -lm


## Run tests in parallel
for model in 0 7 8 14
do
echo "Starting mcstas jobs for model $model"
## Configuration 1
  ./loki-master-postSampleOnly -d "config_1_model_$model" \
        sampletype=$model incohb=0.00001 reardet=5.0 \
        sourceapr=0.015 sampleapr=0.005 \
		inputfile='vsource_config_1/loki_vsource-collen_2.0m-ap1_15mm-ap2_5mm-lmin_3.0A-lmax_11.5A.list'\
        mcplout=$mcplout windows=0 >"config_1_model_$model.log" 2>&1 &

## Configuration 2
  ./loki-master-postSampleOnly -d "config_2_model_$model" \
        sampletype=$model incohb=0.00001 reardet=5.0 \
        sourceapr=0.010 sampleapr=0.005 \
		inputfile='vsource_config_2/loki_vsource-collen_5.0m-ap1_10mm-ap2_5mm-lmin_3.0A-lmax_11.5A.list'\
        mcplout=$mcplout windows=0 >"config_2_model_$model.log" 2>&1 &

## Configuration 3
  ./loki-master-postSampleOnly -d "config_3_model_$model" \
        sampletype=$model incohb=0.00001 reardet=10.0 \
        sourceapr=0.010 sampleapr=0.005 \
		inputfile='vsource_config_3/loki_vsource-collen_10.0m-ap1_10mm-ap2_5mm-lmin_3.0A-lmax_9.7A.list'\
        mcplout=$mcplout windows=0>"config_3_model_$model.log" 2>&1 &

## Configuration 4
   ./loki-master-postSampleOnly -d "config_4_model_$model" \
        sampletype=$model incohb=0.00001 reardet=5.0 \
        sourceapr=0.010 sampleapr=0.005 \
		inputfile='vsource_config_4/loki_vsource-collen_5.0m-ap1_10mm-ap2_5mm-lmin_3.0A-lmax_21.9A.list'\
        mcplout=$mcplout windows=0 >"config_4_model_$model.log" 2>&1 &
echo "Waiting for mcstas jobs for model $model to finish ..."
wait
done
echo " All mcstas jobs finished ..."
#echo "processing output ..."
